import requests
import time

ON_URL = "http://192.168.1.103:3000/plugs/8006B6BB362B3B3FD320043A9D9ABBA91AD76EBC/on"
OFF_URL = "http://192.168.1.103:3000/plugs/8006B6BB362B3B3FD320043A9D9ABBA91AD76EBC/off"
WATCH_URL = "http://missiledefense.ddns.net/api/highscores/"


def check_api_online():
	try:
		resp = requests.get(WATCH_URL)
		resp.raise_for_status
		return True
	except:
		return False


def main():
	while True:
		print("Pinging API...")

		if not check_api_online():
			print("API Offline, attempting to start...")
			requests.get(OFF_URL)
			time.sleep(10)
			requests.get(ON_URL)
			time.sleep(300)
			continue
		else:
			print("API Online")

		time.sleep(3600)


if __name__ == "__main__":
	main()
